interface IDefaultNotification {
    title?: string;
    message?: string;
}

export default IDefaultNotification;