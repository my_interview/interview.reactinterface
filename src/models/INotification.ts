interface INotification {
    id: string;
    message: string;
};

export default INotification;