import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import QuestionsPage from "./pages/QuestionsPage";
import QuestionsAddPage from "./pages/QuestionsAddPage";
import Navbar from "./components/Navbar/Navbar";
import Header from "./components/Header/Header";
import styled from "styled-components";
import TagsPage from "./pages/TagsPage";
import TagsAddPage from "./pages/TagsAddPage";
import ProfilePage from "./pages/ProfilePage";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from ".";
import { loginProfile } from "./services/thunks/profileThunks";
import { ReactNotifications } from "react-notifications-component";
import 'react-notifications-component/dist/theme.css'
import Notifications from "./components/Notifications/Notifications";
import { useSelector } from "react-redux";
import QuestionsSearch from "./pages/QuestionsSearch";

const Inner = styled.div`
    width: 80%;
    min-width: 800px;
    margin: 0 auto;
    background-color: #fff;
    min-height: 100%;
`;

function App() {
    const dispatch = useAppDispatch();

    useEffect(() => {
        const profileId = localStorage.getItem("profileId");

        if (profileId) {
            // called twice in dev mode (strict mode enabled)
            dispatch(loginProfile(profileId));
        }
    }, []);

    return (
        <>
            <Notifications/>
            <Router>
                <Inner>
                    <Header />
                    <Routes>
                        <Route path="/" >
                            <Route index element={<QuestionsPage />} />
                            <Route path=":id" element={<QuestionsPage />} />
                        </Route>
                        <Route path="/profile" element={<ProfilePage />} />
                        <Route path="/questions" >
                            <Route index element={<QuestionsPage />} />
                            <Route path=":id" element={<QuestionsPage />} />
                        </Route>
                        <Route path="/questions/pattern">
                            <Route index element={<QuestionsSearch />} />
                            <Route path=":id" element={<QuestionsSearch />} />
                        </Route>
                        <Route
                            path="/questions/add"
                            element={<QuestionsAddPage />}
                        />
                        <Route path="/tags" element={<TagsPage />} />
                        <Route path="/tags/add" element={<TagsAddPage />} />
                    </Routes>
                </Inner>
            </Router>
        </>
    );
}

export default App;
