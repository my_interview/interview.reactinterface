export function getEnv(envName: string): string {
    if (process.env.NODE_ENV === `production`) {
        return (window as any)._env_[envName];
    }

    const envValue = process.env[envName];

    if (envValue === undefined)
        throw new Error(`Env with name '${envName}' is undefined`);

    return envValue;
}
