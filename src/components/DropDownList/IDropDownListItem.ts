interface IDropDownListItem {
    name: string;
    value: string;
}

export default IDropDownListItem;
