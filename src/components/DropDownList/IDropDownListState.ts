import IDropDownListItem from "./IDropDownListItem";

interface IDropDownListState {
    selectedItem?: IDropDownListItem;
}

export default IDropDownListState;
