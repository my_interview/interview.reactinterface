import IDropDownListItem from "./IDropDownListItem";

interface IDropDownListProps {
    items: Array<IDropDownListItem>;
    onSelect?: (item: IDropDownListItem) => void;
}

export default IDropDownListProps;
