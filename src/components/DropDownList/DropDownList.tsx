import { randomUUID } from "crypto";
import React, { useState } from "react";
import styled from "styled-components";
import IDropDownListProps from "./IDropDownListProps";
import IDropDownListState from "./IDropDownListState";
import { v4 as uuid } from "uuid";
import IDropDownListItem from "./IDropDownListItem";

const Item = styled.div`
    width: 100%;
    border-radius: 3px;
`;
const Inner = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 10px 16px;
    border: 2px solid #333333;
    border-radius: 3px;
    height: 35px;
`;

const ItemWrapper = styled.div`
    margin: 4px 8px;
    width: inherit;
    &:hover > ${Item} {
        color: #ffffff;
        background-color: #333333;
    }
    & > ${Item} {
        padding: 8px 16px;
    }
`;
const ItemList = styled.div`
    margin-top: 16px;
    max-width: 100%;
    max-height: 280px;
    overflow-y: auto;
    overflow-x: hidden;
    background-color: #ffffff;
    z-index: 2;
    border: 2px solid #333333;
    border-radius: 3px;
`;

const ItemListWrapper = styled.div`
    position: absolute;
    visibility: hidden;
    width: 100%;
    max-width: 100%;
    opacity: 0;
    transition: visibility 0s, opacity 0.2s linear;
`;

const Arrow = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    background: url("/icons/arrow.svg");
    transform: rotate(-90deg);
    transition: all 0.2s ease;
    &::after {
        content: "";
        width: 12px;
        height: 7px;
    }
`;
const Container = styled.div`
    display: block;
    color: #333333;
    width: 100%;
    position: relative;
    height: auto;
    &:hover ${ItemListWrapper} {
        visibility: visible;
        opacity: 1;
    }
    &:hover ${Arrow} {
        transform: rotate(0deg);
    }
`;

const DropDownList = (props: IDropDownListProps) => {
    const [state, setState] = useState<IDropDownListState>({});

    const onItemClick = (item: IDropDownListItem) => {
        setState({ ...state, selectedItem: item });

        if (props.onSelect) {
            props.onSelect(item);
        }
    };

    return (
        <Container>
            <Inner>
                <Item>{state.selectedItem?.name ?? "<Не выбрано>"}</Item>
                <Arrow />
            </Inner>
            <ItemListWrapper>
                <ItemList>
                    {props.items.map((i) => (
                        <ItemWrapper
                            key={uuid()}
                            onClick={() => onItemClick(i)}
                        >
                            <Item>{i.name}</Item>
                        </ItemWrapper>
                    ))}
                </ItemList>
            </ItemListWrapper>
        </Container>
    );
};

export default DropDownList;
