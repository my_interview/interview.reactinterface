import { useEffect, useMemo, useState } from "react";
import { ReactNotifications } from "react-notifications-component";
import { JsxEmit } from "typescript";
import { useAppDispatch, useAppSelector } from "../..";
import { NotificationActionTypes } from "../../services/actions/notificationActions";
import { sendErrorNotification, sendInfoNotification, sendSuccessNotification } from "../../services/notifications/defaultNotifications";

const Notifications = () => {
    const {  infoNotifications, successNotifications, errorNotifications  } = useAppSelector(s => s.notificationReducer);
    const dispatch = useAppDispatch();
    const [reactNotifications, _] = useState(useMemo(() => <ReactNotifications />, []));

    useEffect(() => {
        if (infoNotifications.length === 0) return;

        const infoNotification = infoNotifications[0];

        sendInfoNotification({ message: infoNotification.message });

        dispatch({ type: NotificationActionTypes.REMOVE_INFO_NOTIFICATION, notificationId: infoNotification.id })
    }, [infoNotifications]);

    useEffect(() => {
        if (infoNotifications.length === 0) return;

        const infoNotification = infoNotifications[0];

        sendInfoNotification({ message: infoNotification.message });

        dispatch({ type: NotificationActionTypes.REMOVE_INFO_NOTIFICATION, notificationId: infoNotification.id })
    }, [successNotifications]);

    useEffect(() => {
        if (successNotifications.length === 0) return;

        const successNotification = successNotifications[0];

        sendSuccessNotification({ message: successNotification.message });

        dispatch({ type: NotificationActionTypes.REMOVE_SUCCESS_NOTIFICATION, notificationId: successNotification.id })
    }, [successNotifications]);

    useEffect(() => {
        if (errorNotifications.length === 0) return;

        const errorNotification = errorNotifications[0];

        sendErrorNotification({ message: errorNotification.message });

        dispatch({ type: NotificationActionTypes.REMOVE_SUCCESS_NOTIFICATION, notificationId: errorNotification.id })
    }, [errorNotifications]);

    return (
        reactNotifications
    );
};

export default Notifications;