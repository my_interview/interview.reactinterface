import { NavLink } from "react-router-dom";
import styled from "styled-components";

const Container = styled.div`
    display: flex;
    position: relative;
    align-items: center;
    justify-content: space-around;
    width: 100%;
`;
const StyledNavLink = styled(NavLink)`
    text-decoration: none;
    color: inherit;
`;

const Navbar = () => {
    return (
        <Container>
            <StyledNavLink to="/">Главная</StyledNavLink>
            <StyledNavLink to="/profile">Профиль</StyledNavLink>
            <StyledNavLink to="/questions">Вопросы</StyledNavLink>
            <StyledNavLink to="/questions/pattern">Поиск по паттерну</StyledNavLink>
            <StyledNavLink to="/questions/add">Добавить вопрос</StyledNavLink>
            <StyledNavLink to="/tags">Тэги</StyledNavLink>
            <StyledNavLink to="/tags/add">Добавить тэг</StyledNavLink>
        </Container>
    );
};

export default Navbar;
