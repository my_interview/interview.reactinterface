import CardSideEnum from "../QuestionCardSide/CardSideEnum";
import IQuestion from "../../entities/IQuestion";
import FlipperContainer from "../FlipperContainer/FlipperContainer";
import QuestionCardSide from "../QuestionCardSide/QuestionCardSide";
import { useEffect, useState } from "react";
import Modal from "../Modal/Modal";
import { NavLink, useLocation } from "react-router-dom";
import { Link } from "react-router-dom";

const FlipperQuestionCard = (question: IQuestion) => {

    const location = useLocation();

    return (
        <Link to={question.id} state={{ previousRoute: location.pathname }}>
            <FlipperContainer>
                <QuestionCardSide {...question} cardSide={CardSideEnum.Front} />
                <QuestionCardSide {...question} cardSide={CardSideEnum.Back} />
            </FlipperContainer>
        </Link>
    );
};

export default FlipperQuestionCard;
