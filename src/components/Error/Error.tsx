import styled from "styled-components";
import IErrorProps from "./IErrorProps";

const Container = styled.p`
    color: red;
`;

const Error = ({ text }: IErrorProps) => {
    return (
        <Container>{`${text}`}</Container>
    );
};

export default Error;