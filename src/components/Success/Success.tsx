import styled from "styled-components";
import ISuccessProps from "./ISuccessProps";

const Container = styled.p`
    color: green;
`;

const Success = ({ message }: ISuccessProps) => {
    return (
        <Container>{message}</Container>
    );
}

export default Success;