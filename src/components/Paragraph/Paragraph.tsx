import styled from "styled-components";
import IParagraphProps from "./IParagraphProps";

const Container = styled.p`
    font-family: "Inter", sans-serif;
    font-weight: 600;
    color: #333333;
`;

const Paragraph = ({ text }: IParagraphProps) => {
    return (
        <Container>{text}</Container>
    );
};

export default Paragraph;