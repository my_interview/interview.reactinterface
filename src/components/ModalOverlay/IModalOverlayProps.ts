import React, { MouseEventHandler } from "react";

export interface IModalOverlayProps {
  onClick: React.MouseEventHandler<HTMLDivElement>;
}