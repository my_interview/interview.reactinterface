import { ForwardedRef, forwardRef } from "react";
import { memo } from "react";
import styled from "styled-components";
import { IModalOverlayProps } from "./IModalOverlayProps";

const Container = styled.div`
    position: fixed;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.4);
    overflow: hidden;
    display: flex;
    align-items: center;
    width: 100%;
    left: 0;
    top: 0;
`;

const ModalOverlay = memo(
    forwardRef(
        (
            { onClick }: IModalOverlayProps,
            ref: ForwardedRef<HTMLDivElement>
        ): JSX.Element => {
            return <Container ref={ref} onClick={onClick} />;
        }
    )
);

export default ModalOverlay;
