import ModalOverlay from "../ModalOverlay/ModalOverlay";
import ReactDOM from "react-dom";
import { Link, Location, useLocation, useNavigate } from "react-router-dom";
import React, { useCallback } from "react";
import { useEffect, useRef } from "react";
import { IChildren } from "../../models/IChildren";
import ILocationState from "../../models/ILocationState";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClose } from "@fortawesome/free-solid-svg-icons";

const Container = styled.div`
    position: fixed;
    height: 100vh;
    background-color: transparent;
    width: 100%;
    z-index: 100;
    display: flex;
    align-items: center;
    max-height: 100vh;
`;

const ContentWrapper = styled.div`
    width: 720px;
    margin: 0 auto;
    background-color: #ffffff;
    border-radius: 40px;
    position: relative;
`;

const Content = styled.div`
    margin: 20px;
    overflow: hidden;
    overflow-wrap: break-word;
`;

const ButtonLink = styled(Link)`
    position: absolute;
    right: 0;
    cursor: pointer;
    height: 24px;
    width: 24px;
    z-index: 100;
`;

const Icon = styled(FontAwesomeIcon)`
    color: #333333;
`;

const Modal = ({ children }: IChildren): JSX.Element => {
    const location = useLocation();
    const navigate = useNavigate();
    const keyCodeEsc: number = 27;
    const overlayRef = useRef<HTMLDivElement>(null);

    const closeOnClick = useCallback<React.MouseEventHandler<HTMLDivElement>>(
        (e: React.MouseEvent) => {
            if (e.target === overlayRef.current) {
                navigate(-1);
            }
        },
        [navigate, overlayRef]
    );

    const closeOnPress = useCallback<any>(
        (e: any) => {
            if (e.keyCode === keyCodeEsc) {
                navigate(-1);
            }
        },
        [navigate]
    );

    useEffect(() => {
        document.addEventListener("keydown", closeOnPress);

        return () => {
            document.removeEventListener("keydown", closeOnPress);
        };
    }, [closeOnPress]);

    useEffect(() => {
        console.log(location.pathname);
    }, [location.pathname]);

    return ReactDOM.createPortal(
        <Container>
            <ModalOverlay ref={overlayRef} onClick={closeOnClick} />
            <ContentWrapper>
                <Content>
                    <ButtonLink
                        to={
                            (location.state as ILocationState)?.previousRoute ??
                            "/"
                        }
                    >
                    <Icon icon={faClose} size={"2x"} />
                    </ButtonLink>
                    {children}
                </Content>
            </ContentWrapper>
        </Container>,
        document.getElementById("modal") as HTMLElement
    );
};

export default Modal;
