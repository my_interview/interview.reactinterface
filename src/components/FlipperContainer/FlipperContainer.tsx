import styled from "styled-components";

const Side = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: 1s;
    backface-visibility: hidden;
    transform-style: preserve-3d;
`;
const FrontSide = styled(Side)``;
const BackSide = styled(Side)`
    transform: rotateY(180deg);
`;

const InvisibleSide = styled.div`
    visibility: hidden;
`;
const Container = styled.div`
    perspective: 1000px;

    &:hover ${FrontSide} {
        transform: rotateY(180deg);
    }

    &:hover ${BackSide} {
        transform: rotateY(360deg);
    }
`;

const FlipperContainer = (props: any) => {
    return (
        <Container onClick={props.onClick}>
            <InvisibleSide>{props.children[0]}</InvisibleSide>
            <FrontSide>{props.children[0]}</FrontSide>
            <BackSide>{props.children[1]}</BackSide>
        </Container>
    );
};

export default FlipperContainer;
