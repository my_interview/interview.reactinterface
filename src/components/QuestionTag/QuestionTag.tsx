import styled from "styled-components";
import IQuestionTagProps from "./IQuestionTagProps";
import ITag from "./IQuestionTagProps";

const Container = styled.span`
    padding: 3px;
    background-color: #333333;
    border: 2px solid #333333;
    border-radius: 30px;
    color: #ffffff;
    user-select: none;
    &:hover {
        background-color: #ffffff;
        color: #333333;
    }
`;

const QuestionTag = ({ tag, onClick }: IQuestionTagProps) => {
    return (
        <Container onClick={() => onClick(tag)}>{tag.name}</Container>
    );
};

export default QuestionTag;