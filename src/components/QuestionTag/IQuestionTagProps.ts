import ITag from "../../entities/ITag";

interface IQuestionTagProps {
    tag: ITag,
    onClick: (tag: ITag) => void
}

export default IQuestionTagProps;