import styled from "styled-components";
import ITextProps from "./ITextProps";

const P = styled.p`
    color: #aaa;
`;

const Text = ({ text }: ITextProps) => {
    return (
        <P>{text}</P>
    )
}

export default Text;