interface IPaginatorProps {
    pageCount: number;
    pageLinks: number;
    onMove: (p: number) => void;
}

export default IPaginatorProps;
