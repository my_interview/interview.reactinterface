import { useEffect, useState } from "react";
import styled from "styled-components";
import IPaginatorProps from "./IPaginatorProps";

const Container = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 10px;
`;
const PaginatorItem = styled.button`
    width: 40px;
    height: 30px;
    background-color: transparent;
    border: 2px solid #333333;
    border-radius: 15px;
`;
const SelectedPaginatorItem = styled(PaginatorItem)`
    background-color: #333333;
    color: #ffffff;
`;

const Paginator = (props: IPaginatorProps) => {
    const [currentPage, setCurrentPage] = useState<number>(1);
    const [paginatorPages, setPaginatorPages] = useState<Array<number>>();
    useEffect(() => {
        const paginatorPagesCount =
            props.pageCount > props.pageLinks
                ? props.pageLinks
                : props.pageCount;

        const paginatorPages = new Array<number>();
        let term = 1;

        console.log(paginatorPagesCount);
        
        for (let i = 0; i < Math.floor(paginatorPagesCount / 2); i++) {
            const offset = i + term;

            if (
                currentPage - offset < 1 &&
                currentPage + offset <= props.pageCount
            ) {
                paginatorPages.push(currentPage + offset);

                if (currentPage + offset + 1 <= props.pageCount) {
                    paginatorPages.push(currentPage + offset + 1);
                }

                term++;

                continue;
            }

            if (
                currentPage - offset >= 1 &&
                currentPage + offset > props.pageCount
            ) {
                paginatorPages.push(currentPage - offset);

                if (currentPage - offset - 1 > 0) {
                    paginatorPages.push(currentPage - offset - 1);
                }

                term++;

                continue;
            }

            if (currentPage - offset < 1) {
                continue;
            }

            if (currentPage + offset > props.pageCount) {
                continue;
            }

            paginatorPages.push(currentPage + offset);
            paginatorPages.push(currentPage - offset);
        }

        paginatorPages.push(currentPage);
        paginatorPages.sort((a, b) => a - b);

        setPaginatorPages(paginatorPages);
    }, [props.pageCount, currentPage]);

    const handleMove = (p: number) => {
        setCurrentPage(p);
        props.onMove(p);
    };

    return (
        <Container>
            {props.pageCount > props.pageLinks && (
                <PaginatorItem onClick={() => handleMove(1)}>
                    {"<<"}
                </PaginatorItem>
            )}
            {paginatorPages?.map((x) =>
                x === currentPage ? (
                    <SelectedPaginatorItem
                        key={x}
                        onClick={() => handleMove(x)}
                    >
                        {x}
                    </SelectedPaginatorItem>
                ) : (
                    <PaginatorItem key={x} onClick={() => handleMove(x)}>
                        {x}
                    </PaginatorItem>
                )
            )}
            {props.pageCount > props.pageLinks && (
                <PaginatorItem onClick={() => handleMove(props.pageCount)}>
                    {">>"}
                </PaginatorItem>
            )}
        </Container>
    );
};

export default Paginator;
