import styled from "styled-components";
import IInputProps from "./IInputProps";

const Container = styled.input`
    font-family: "Inter", sans-serif;
    font-weight: 600;
    color: #333333;
    width: 100%;
    border: 2px solid #333333;
    border-radius: 3px;
    padding: 8px 16px;
    height: 35px;
    outline: none;
`;

const Input = (props: IInputProps) => {
    return (
        <Container required={props.required} maxLength={props.maxLength} placeholder={props.placeholder} type={props.type} value={props.value} name={props.name} onChange={props.onChange} />
    );
};

export default Input;