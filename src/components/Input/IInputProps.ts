interface IInputProps {
  type?: React.HTMLInputTypeAttribute;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  name?: string;
  value?: string | number | readonly string[];
  placeholder?: string;
  required?: boolean;
  maxLength?: number;
}

export default IInputProps;
