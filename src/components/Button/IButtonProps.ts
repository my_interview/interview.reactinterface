import React, { ButtonHTMLAttributes } from "react";

interface IButtonProps {
    text: string;
    name?: string;
    type?: "button" | "submit" | "reset",
    onClick?: React.MouseEventHandler<HTMLButtonElement>
}

export default IButtonProps;