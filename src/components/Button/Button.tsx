import styled from "styled-components";
import IButtonProps from "./IButtonProps";

const Container = styled.button`
    background-color: #333333;
    color: #ffffff;
    font-family: "Inter", sans-serif;
    font-weight: 600;
    padding: 13px 32px;
    border-radius: 100px;
    height: auto;
    border-color: transparent;
    &:hover {
        color: #333333;
        background-color: #ffffff;
        border: 2px solid #333333;
    }
    align-self: center;
`;

const Button = ({ name, text, type, onClick }: IButtonProps) => {
    return (
        <Container type={type} onClick={onClick} name={name}>{text}</Container>
    );
};

export default Button;
