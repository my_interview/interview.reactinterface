import styled from "styled-components";
import IQuestionCardGridProps from "./IQuestionCardGridProps";
import IQuestionCardSideProps from "../QuestionCardSide/IQuestionCardSideProps";
import FlipperQuestionCard from "../FlipperQuestionCard/FlipperQuestionCard";

const Grid = styled.div`
    display: grid;
    justify-content: center;
    grid-template-columns: 45% 45%;
    gap: 15px;
    padding: 10px;
`;

const QuestionCardGrid = (props: IQuestionCardGridProps) => {
    return (
        <Grid>
            {props.questions.map((q) => (
                <FlipperQuestionCard key={q.id} {...q} />
            ))}
        </Grid>
    );
};

export default QuestionCardGrid;
