import IQuestion from "../../entities/IQuestion";

interface IQuestionCardGridProps {
    questions: Array<IQuestion>;
}

export default IQuestionCardGridProps;
