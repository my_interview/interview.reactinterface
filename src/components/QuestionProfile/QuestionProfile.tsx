import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "../..";
import IQuestion from "../../entities/IQuestion";
import {
    deleteQuestion,
    fetchTargetQuestion,
    postQuestion,
    updateQuestion,
} from "../../services/thunks/questionThunks";
import Button from "../Button/Button";
import QuestionEditForm from "../QuestionEditForm/QuestionEdit";
import IQuestionProfileParams from "./IQuestionProfileParams";

const Container = styled.div`
    padding: 20px;
`;

const QuestionProfile = () => {
    const { targetQuestion } = useAppSelector((s) => s.questionReducer);
    const { profileId } = useAppSelector((s) => s.profileReducer);
    const { id } = useParams<keyof IQuestionProfileParams>();
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (!id) return;

        dispatch(fetchTargetQuestion(id, profileId));
    }, [dispatch, id, profileId]);

    const handleEdit = (question: IQuestion) => {
        dispatch(updateQuestion(question));
    };

    const handleDelete = (questionId: string) => {
        if (profileId) {
            dispatch(deleteQuestion(questionId, profileId));
        }
    };

    return (
        <Container>
            {!targetQuestion ? (
                <p>Вопрос не найден</p>
            ) : (
                <QuestionEditForm
                    handleEdit={handleEdit}
                    handleDelete={handleDelete}
                    editableQuestion={targetQuestion}
                />
            )}
        </Container>
    );
};

export default QuestionProfile;
