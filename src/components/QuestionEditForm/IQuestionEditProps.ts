import IQuestion from "../../entities/IQuestion";

interface IQuestionEditProps {
    handleCreate?: (question: IQuestion) => void,
    handleEdit?: (question: IQuestion) => void,
    handleDelete?: (questionId: string) => void,
    editableQuestion?: IQuestion,
}

export default IQuestionEditProps;