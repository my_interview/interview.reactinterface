import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "../..";
import IQuestion from "../../entities/IQuestion";
import ITag from "../../entities/ITag";
import { QuestionActionTypes } from "../../services/actions/questionActions";
import { updateQuestion } from "../../services/thunks/questionThunks";
import { deleteTag, fetchAllTags } from "../../services/thunks/tagThunks";
import Button from "../Button/Button";
import DropDownList from "../DropDownList/DropDownList";
import IDropDownListItem from "../DropDownList/IDropDownListItem";
import Input from "../Input/Input";
import Paragraph from "../Paragraph/Paragraph";
import QuestionTag from "../QuestionTag/QuestionTag";
import Success from "../Success/Success";
import TextArea from "../TextArea/TextArea";
import IQuestionEditProps from "./IQuestionEditProps";

const BlockLabel = styled.label`
    display: block;
`;
const Form = styled.form`
    display: flex;
    flex-direction: column;
    gap: 15px;
`;
const Tags = styled.div`
    display: flex;
    gap: 10px;
`;

const QuestionEditForm = ({
    editableQuestion,
    handleCreate,
    handleEdit,
    handleDelete,
}: IQuestionEditProps) => {
    const dispatch = useAppDispatch();
    const profileId = useAppSelector((s) => s.profileReducer.profileId);
    const tags = useAppSelector((s) => s.tagReducer.tags);
    const [dropdownItems, setDropdownItems] = useState<
        Array<IDropDownListItem>
    >([]);
    const { postQuestionSuccess } =
        useAppSelector((s) => s.questionReducer);
    const getDefaultQuestion = (): IQuestion => {
        return {
            id: "",
            answer: "",
            content: "",
            profileId: "",
            tags: new Array<ITag>(),
            isOwner: false,
        };
    };
    const [question, setQuestion] = useState<IQuestion>(getDefaultQuestion());
    const onSelectTag = (item: IDropDownListItem) => {
        if (question.tags.some((x) => x.name === item.name)) return;

        setQuestion({
            ...question,
            tags: [...question.tags, { name: item.name, id: item.value }],
        });
    };

    useEffect(() => {
        setQuestion({ ... editableQuestion || getDefaultQuestion(), profileId: profileId ?? "" });
    }, [editableQuestion]);

    useEffect(() => {
        dispatch(fetchAllTags());
    }, [dispatch]);

    useEffect(() => {
        setDropdownItems(
            tags.map((tag) => ({ name: tag.name, value: tag.id }))
        );
    }, [tags]);

    useEffect(() => {
        setQuestion({ ...question, profileId: profileId ?? "" });
    }, [profileId]);

    useEffect(() => {
        if (!postQuestionSuccess) return;

        setQuestion(getDefaultQuestion());
    }, [postQuestionSuccess]);

    const onChange = (
        e: React.ChangeEvent<{ name: string; value: string }>
    ) => {
        setQuestion({
            ...question,
            [e.target.name]: e.target.value,
        });

        dispatch({
            type: QuestionActionTypes.CLEAR_POST_QUESTION_NOTIFICATIONS,
        });
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const nativeEvent = e.nativeEvent as SubmitEvent;
        const submitter = nativeEvent.submitter as HTMLButtonElement;

        if (submitter.name === "create") {
            handleCreate && handleCreate(question);

            return;
        }

        if (submitter.name === "edit") {
            handleEdit && handleEdit(question);

            return;
        }

        if (submitter.name === "delete" && editableQuestion) {
            console.log(handleDelete);
            handleDelete && handleDelete(editableQuestion.id);

            return;
        }
    };

    const handleRemoveTag = (tag: ITag) => {
        setQuestion({ ...question, tags: question.tags.filter(t => t.id !== tag.id) });
    }

    return (
        <Form onSubmit={handleSubmit}>
            {!editableQuestion && (
                <BlockLabel>
                    <Paragraph text="Идентификатор профиля" />
                    <Input
                        type="text"
                        onChange={onChange}
                        name="profileId"
                        value={question.profileId}
                        placeholder="b2812441-5114-4a5c-925e-d89e78388b98"
                    ></Input>
                </BlockLabel>
            )}
            <div>
                Добавленные тэги: <Tags>{question.tags.map((tag) => <QuestionTag key={tag.id} tag={tag} onClick={handleRemoveTag} />)}</Tags>
            </div>
            {handleEdit && (
                <BlockLabel>
                    <Paragraph text="Тэг" />
                    <DropDownList
                        items={dropdownItems}
                        onSelect={(item) => onSelectTag(item)}
                    ></DropDownList>
                </BlockLabel>
            )}
            <BlockLabel>
                <Paragraph text="Вопрос" />
                <TextArea
                    onChange={onChange}
                    name="content"
                    value={question.content}
                    placeholder="Основные принципы ООП"
                ></TextArea>
            </BlockLabel>
            <BlockLabel>
                <Paragraph text="Ответ" />
                <TextArea
                    onChange={onChange}
                    name="answer"
                    value={question.answer}
                    placeholder="абстракция, инкапсуляция, наследование, полиморфизм"
                ></TextArea>
            </BlockLabel>

            {handleCreate && (
                <Button name="create" type="submit" text="Создать" />
            )}
            {handleEdit && (
                <Button name="edit" type="submit" text="Сохранить" />
            )}
            {handleDelete && (
                <Button name="delete" type="submit" text="Удалить" />
            )}
        </Form>
    );
};

export default QuestionEditForm;
