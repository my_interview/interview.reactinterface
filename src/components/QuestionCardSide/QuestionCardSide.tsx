import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import IQuestion from "../../entities/IQuestion";
import {
    faQuestionCircle,
    faExclamationCircle,
} from "@fortawesome/free-solid-svg-icons";
import IQuestionCardSideProps from "./IQuestionCardSideProps";
import CardSideEnum from "./CardSideEnum";
import Title from "../Title/Title";
import Text from "../Text/Text";

const Container = styled.div`
    background-color: #ffffff;
    border: 2px solid #333333;
    border-radius: 15px;
    width: 100%;
    height: 150px;
    perspective: 1200;
    transform-style: preserve-3d;
`;

const Inner = styled.div`
    padding: 25px;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

const LimitedContainer = styled.div`
    width: 70%;
    overflow: hidden;
`;

const Icon = styled(FontAwesomeIcon)`
    color: #333333;
`;

const QuestionCardSide = (props: IQuestionCardSideProps) => {
    return (
        <Container>
            <Inner>
                {props.cardSide == CardSideEnum.Front ? (
                    <>
                        <LimitedContainer>
                            <Title text="Вопрос"/>
                            <Text text={props.content} />
                        </LimitedContainer>
                        <Icon icon={faQuestionCircle} size={"5x"} />
                    </>
                ) : (
                    <>
                        <LimitedContainer>
                            <Title text="Ответ" />
                            <Text text={props.answer} />
                        </LimitedContainer>
                        <Icon icon={faExclamationCircle} size={"5x"} />
                    </>
                )}
            </Inner>
        </Container>
    );
};

export default QuestionCardSide;
