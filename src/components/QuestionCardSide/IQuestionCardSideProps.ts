import CardSideEnum from "./CardSideEnum";
import IQuestion from "../../entities/IQuestion";

interface IQuestionCardSideProps extends IQuestion {
    cardSide: CardSideEnum;
}

export default IQuestionCardSideProps;
