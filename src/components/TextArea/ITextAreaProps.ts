interface ITextAreaProps {
    onChange?: React.ChangeEventHandler<HTMLTextAreaElement>;
    name?: string;
    value?: string | number | readonly string[];
    placeholder?: string;
    required?: boolean;
    maxLength?: number;
}

export default ITextAreaProps;