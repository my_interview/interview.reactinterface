import styled from "styled-components";
import ITextAreaProps from "./ITextAreaProps";

const Container = styled.textarea`
    font-family: "Inter", sans-serif;
    font-weight: 600;
    color: #333333;
    width: 100%;
    max-width: 100%;
    border: 2px solid #333333;
    border-radius: 3px;
    padding: 8px 16px;
    height: 100%;
    min-height: 70px;
    outline: none;
`;

const TextArea = (props: ITextAreaProps) => {
    return (
        <Container
            name={props.name}
            onChange={props.onChange}
            maxLength={props.maxLength}
            required={props.required}
            placeholder={props.placeholder}
            value={props.value}
        ></Container>
    );
};

export default TextArea;
