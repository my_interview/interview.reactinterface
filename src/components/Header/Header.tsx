import styled from "styled-components";
import Navbar from "../Navbar/Navbar";

const Container = styled.div`
    height: 50px;
    width: 100%;
    background-color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const Header = () => {
    return (
        <Container>
            <p>Тут будет лого</p>
            <Navbar />
        </Container>
    );
};

export default Header;
