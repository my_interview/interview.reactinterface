import styled from "styled-components";
import { IChildren } from "../../models/IChildren";
import ITitleProps from "./ITitleProps";

const P = styled.p`
    font-weight: bold;
    color: black;
`;

const Title = ({ text }: ITitleProps) => {
    return (
        <P>{text}</P>
    )
}

export default Title;