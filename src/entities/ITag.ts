interface ITag {
    id: string,
    name: string;
}

export default ITag;
