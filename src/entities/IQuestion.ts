import uuid from "uuid";
import ITag from "./ITag";

interface IQuestion {
    id: string;
    content: string;
    answer: string;
    tags: Array<ITag>;
    profileId: string;
    isOwner?: boolean;
}

export default IQuestion;
