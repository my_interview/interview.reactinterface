import React from "react";
import { Action, AnyAction } from "redux";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import {
    createStoreHook,
    Provider,
    TypedUseSelectorHook,
    useDispatch,
    useSelector,
} from "react-redux";
import { rootReducer, RootState } from "./services/reducers/rootReducer";
import { configureStore, ThunkAction } from "@reduxjs/toolkit";
import { ThunkDispatch } from "@reduxjs/toolkit";

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
);
const store = configureStore({ reducer: rootReducer });
//export type ApplicationStore = typeof store;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    AnyAction
>;
export type AppDispatch = ThunkDispatch<RootState, void, Action>;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
