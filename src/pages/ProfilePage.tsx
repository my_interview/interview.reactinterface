import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "..";
import Button from "../components/Button/Button";
import Input from "../components/Input/Input";
import Paragraph from "../components/Paragraph/Paragraph";
import { ProfileActionTypes } from "../services/actions/profileActions";
import { createProfile, loginProfile } from "../services/thunks/profileThunks";

const Container = styled.div`
    width: 400px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    gap: 30px;
`;

const Form = styled.form`
    display: flex;
    width: 100%;
    flex-direction: column;
    gap: 10px;
    text-align: start;
`;

const ProfilePage = () => {
    const profileId = useAppSelector(s => s.profileReducer.profileId);
    const [inputProfileId, setInputProfileId] = useState<string>('');
    const dispatch = useAppDispatch();

    const handleChangeProfileId = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputProfileId(e.target.value);
    };
    const handleCreateProfile = () => {
        dispatch(createProfile());
    };
    const handleLogin = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        dispatch(loginProfile(inputProfileId));
    };
    const handleLogout = () => {
        dispatch({ type: ProfileActionTypes.LOGOUT_PROFILE });
    };

    return (
        <Container>
            {profileId ? (
                <>
                    <Paragraph text={`Ваш профиль: ${profileId}`} />
                    <Button onClick={handleLogout} text="Выйти" />
                </>
            ) : (
                <>
                    <div>
                        <Paragraph text="Ещё не создан профиль?" />
                        <Button onClick={handleCreateProfile} text="Создать профиль" />
                    </div>
                    <Form onSubmit={handleLogin}>
                        <label>
                            <Paragraph text="Профиль уже создан?" />
                            <Input type="text" onChange={handleChangeProfileId} />
                        </label>
                        <Button type="submit" text="Войти" />
                    </Form>
                </>
            )}
        </Container>
    );
};

export default ProfilePage;
