import { useEffect } from "react";
import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "..";
import QuestionTag from "../components/QuestionTag/QuestionTag";
import ITag from "../entities/ITag";
import { deleteTag, fetchAllTags } from "../services/thunks/tagThunks";

const TagWrapper = styled.div`
    display: inline-block;
    margin: 5px;
`;

const TagsPage = () => {
    const dispatch = useAppDispatch();
    const tags = useAppSelector((s) => s.tagReducer.tags);

    useEffect(() => {
        dispatch(fetchAllTags());
    }, []);

    const handleDeleteTag = (tag: ITag) => {
        dispatch(deleteTag(tag.id));
    };

    return (
        <>
            {tags.map((t) => (
                <TagWrapper key={t.id}>
                    <QuestionTag tag={t} onClick={handleDeleteTag} />
                </TagWrapper>
            ))}
        </>
    );
};

export default TagsPage;
