import { createBrowserHistory } from "history";
import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes, useLocation, useMatch, useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "..";
import Modal from "../components/Modal/Modal";
import Paginator from "../components/Paginator/Paginator";
import QuestionCardGrid from "../components/QuestionCardGrid/QuestionCardGrid";
import QuestionProfile from "../components/QuestionProfile/QuestionProfile";
import {
    fetchQuestionsByPage,
    fetchTargetQuestion,
    getPageCount,
} from "../services/thunks/questionThunks";

const QuestionsPage = () => {
    const { questions, pagesCount } = useAppSelector((s) => s.questionReducer);
    const [currentPage, setCurrentPage] = useState<number>(1);
    const dispatch = useAppDispatch();
    const { id } = useParams<string>();

    useEffect(() => {
        dispatch(fetchQuestionsByPage(currentPage));
    }, [dispatch, currentPage]);    
    
    useEffect(() => {
        dispatch(getPageCount());
    }, [dispatch]);


    return (
        <>
            { id && <Modal><QuestionProfile/></Modal> }

            <Paginator
                pageCount={pagesCount}
                pageLinks={9}
                onMove={(x) => setCurrentPage(x)}
            />
            <QuestionCardGrid questions={questions} />

        </>
    );
};

export default QuestionsPage;
