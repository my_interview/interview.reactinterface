import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { setConstantValue } from "typescript";
import { useAppDispatch, useAppSelector } from "..";
import Button from "../components/Button/Button";
import DropDownList from "../components/DropDownList/DropDownList";
import IDropDownListItem from "../components/DropDownList/IDropDownListItem";
import Error from "../components/Error/Error";
import Input from "../components/Input/Input";
import Paragraph from "../components/Paragraph/Paragraph";
import QuestionEditForm from "../components/QuestionEditForm/QuestionEdit";
import Success from "../components/Success/Success";
import TextArea from "../components/TextArea/TextArea";
import IQuestion from "../entities/IQuestion";
import ITag from "../entities/ITag";
import { QuestionActionTypes } from "../services/actions/questionActions";
import { postQuestion } from "../services/thunks/questionThunks";
import { fetchAllTags } from "../services/thunks/tagThunks";

const Container = styled.div`
    margin: auto auto;
    width: 100%;
    & > form {
        margin: 0 auto;
        width: 300px;
    }
`;

const QuestionsAddPage = () => {

    const dispatch = useAppDispatch();
    const onSubmit = (question: IQuestion) => {
        dispatch(postQuestion(question));
    };

    return (
        <Container>
            <QuestionEditForm handleEdit={onSubmit}/>
        </Container>
    );
};

export default QuestionsAddPage;
