import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "..";
import Input from "../components/Input/Input";
import Modal from "../components/Modal/Modal";
import QuestionCardGrid from "../components/QuestionCardGrid/QuestionCardGrid";
import QuestionProfile from "../components/QuestionProfile/QuestionProfile";
import { fetchQuestionsByPattern } from "../services/thunks/questionThunks";

const QuestionsSearch = () => {
    const dispatch = useAppDispatch();
    const questions = useAppSelector(s => s.questionReducer.questionsByPattern);
    const { id } = useParams<string>();
    const fetchQuestions = (e: React.ChangeEvent<{ name: string; value: string }>) => {
        dispatch(fetchQuestionsByPattern(e.target.value));
    }

    return (
        <>
            { id && <Modal><QuestionProfile/></Modal> }
            <Input placeholder="Вопрос" onChange={fetchQuestions} type={"text"} />

            <QuestionCardGrid questions={questions} />
        </>
    );
};

export default QuestionsSearch;