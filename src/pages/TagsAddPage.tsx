import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "..";
import Button from "../components/Button/Button";
import Error from "../components/Error/Error";
import Input from "../components/Input/Input";
import Paragraph from "../components/Paragraph/Paragraph";
import Success from "../components/Success/Success";
import { TagActionTypes } from "../services/actions/tagActions";
import { fetchAllTags, postTag } from "../services/thunks/tagThunks";

const Form = styled.form`
    width: 300px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 20px;
`;

const TagsAddPage = () => {
    const [tagName, setTagName] = useState<string>('');
    const [isExistsTag, setIsExistsTag] = useState<boolean>(false);
    const { postTagFailed, postTagError, tags, lastSuccessPostedTag } = useAppSelector(s => s.tagReducer);
    const dispatch = useAppDispatch();
    const handleChangeTagName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTagName(e.target.value);

        dispatch({ type: TagActionTypes.CLEAR_POST_TAG_NOTIFICATIONS });
    }
    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!isExistsTag) dispatch(postTag(tagName));
    }

    useEffect(() => {
        dispatch(fetchAllTags());
    }, [dispatch]);

    useEffect(() => {
        setIsExistsTag(tags.some(x => x.name === tagName));
    }, [tags])

    return (
        <Form onSubmit={handleSubmit}>
            { postTagFailed && <Error text={postTagError}/> }
            { lastSuccessPostedTag !== tagName && isExistsTag && <Error text={`Тэг с названием ${tagName} уже существует`}/> }
            <label>
                <Paragraph text="Название тэга"/>
                <Input required={true} maxLength={25} type="text" value={tagName} onChange={handleChangeTagName}/>
            </label>
            <Button type="submit" text="Создать"/>
        </Form>
    );
};

export default TagsAddPage;
