import { AppDispatch, AppThunk } from "../..";
import IQuestion from "../../entities/IQuestion";
import { NotificationAction } from "../actions/notificationActions";
import { QuestionAction } from "../actions/questionActions";
import ThunkHandlerDeleteQuestion from "../thunkHandlers/questionThunkHandlers/ThunkHandlerDeleteQuestion";
import ThunkHandlerFetchTargetQuestion from "../thunkHandlers/questionThunkHandlers/ThunkHandlerFetchTargetQuestion";
import ThunkHandlerGetPageCount from "../thunkHandlers/questionThunkHandlers/ThunkHandlerGetPageCount";
import ThunkHandlerPostQuestion from "../thunkHandlers/questionThunkHandlers/ThunkHandlerPostQuestion";
import ThunkHandlerFetchQuestionsByPage from "../thunkHandlers/questionThunkHandlers/ThunkHandlerFetchQuestionsByPage";
import ThunkHandlerUpdateQuestion from "../thunkHandlers/questionThunkHandlers/ThunkHandlerUpdateQuestion";
import ThunkHandlerFetchQuestionsByPattern from "../thunkHandlers/questionThunkHandlers/ThunkHandlerFetchQuestionsByPattern";

export const fetchQuestionsByPage = (page: number): AppThunk => {
  return function (
    dispatch: (action: QuestionAction | NotificationAction) => void
  ) {
    const handler = new ThunkHandlerFetchQuestionsByPage(dispatch, page);

    handler.Handle();
  };
};

export const postQuestion = (question: IQuestion): AppThunk => {
  return async function (
    dispatch: (action: QuestionAction | NotificationAction) => AppDispatch
  ) {
    const handler = new ThunkHandlerPostQuestion(dispatch, question);

    await handler.Handle();
  };
};

export const getPageCount = (): AppThunk => {
  return async function (
    dispatch: (action: QuestionAction | NotificationAction) => void
  ) {
    const handler = new ThunkHandlerGetPageCount(dispatch);

    await handler.Handle();
  };
};

export const fetchTargetQuestion = (
  questionId: string,
  profileId?: string
): AppThunk => {
  return async function (
    dispatch: (action: QuestionAction | NotificationAction) => VoidFunction
  ) {
    const handler = new ThunkHandlerFetchTargetQuestion(
      dispatch,
      questionId,
      profileId
    );

    await handler.Handle();
  };
};

export const deleteQuestion = (
  questionId: string,
  profileId: string
): AppThunk => {
  return async function (
    dispatch: (action: QuestionAction | NotificationAction) => void
  ) {
    const handler = new ThunkHandlerDeleteQuestion(
      dispatch,
      questionId,
      profileId
    );

    await handler.Handle();
  };
};

export const updateQuestion = (
    question: IQuestion
  ): AppThunk => {
    return async function (
      dispatch: (action: QuestionAction | NotificationAction) => void
    ) {
      const handler = new ThunkHandlerUpdateQuestion(dispatch, question);
  
      await handler.Handle();
    };
  };

export const fetchQuestionsByPattern =(pattern: string): AppThunk => {
    return async function (
        dispatch: (action: QuestionAction | NotificationAction) => void
      ) {
        const handler = new ThunkHandlerFetchQuestionsByPattern(dispatch, pattern);
    
        await handler.Handle();
      };
}