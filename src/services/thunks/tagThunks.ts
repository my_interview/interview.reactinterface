import { AppDispatch, AppThunk } from "../..";
import ITag from "../../entities/ITag";
import { getEnv } from "../../utils/envService";
import { NotificationAction } from "../actions/notificationActions";
import { TagAction, TagActionTypes } from "../actions/tagActions";
import ThunkHandlerDeleteTag from "../thunkHandlers/tagThunkHandlers/ThunkHandlerDeleteTag";
import ThunkHandlerFetchAllTags from "../thunkHandlers/tagThunkHandlers/ThunkHandlerFetchAllTags";
import ThunkHandlerPostTag from "../thunkHandlers/tagThunkHandlers/ThunkHandlerPostTag";

export const fetchAllTags = (): AppThunk => {
  return function (
    dispatch: (arg: TagAction | NotificationAction) => AppDispatch
  ) {
    const handler = new ThunkHandlerFetchAllTags(dispatch);

    handler.Handle();
  };
};

export const postTag = (tagName: string): AppThunk => {
  return function (
    dispatch: (arg: TagAction | NotificationAction) => AppDispatch
  ) {
    const handler = new ThunkHandlerPostTag(dispatch, tagName);

    handler.Handle();
  };
};

export const deleteTag = (tagId: string): AppThunk => {
  return function (
    dispatch: (arg: TagAction | NotificationAction) => AppDispatch
  ) {
    const handler = new ThunkHandlerDeleteTag(dispatch, tagId);

    handler.Handle();
  };
};
