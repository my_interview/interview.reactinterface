import { AppDispatch, AppThunk } from "../..";
import { getEnv } from "../../utils/envService";
import { NotificationAction } from "../actions/notificationActions";
import { ProfileAction, ProfileActionTypes } from "../actions/profileActions";
import ThunkHandlerCreateProfile from "../thunkHandlers/profileThunkHandlers/ThunkHandlerCreateProfile";
import ThunkHandlerLoginProfile from "../thunkHandlers/profileThunkHandlers/ThunkHandlerLoginProfile";

export const createProfile = (): AppThunk => {
  return async function (
    dispatch: (arg: ProfileAction | NotificationAction) => AppDispatch
  ) {
    const handler = new ThunkHandlerCreateProfile(dispatch);

    await handler.Handle();
  };
};

export const loginProfile = (profileId: string): AppThunk => {
  return function (
    dispatch: (arg: ProfileAction | NotificationAction) => AppDispatch
  ) {
    const handler = new ThunkHandlerLoginProfile(dispatch, profileId);

    handler.Handle();
  };
};
