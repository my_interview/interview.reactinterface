import INotification from "../../models/INotification";

export enum NotificationActionTypes {
  ADD_INFO_NOTIFICATION = "ADD_INFO_NOTIFICATION",
  REMOVE_INFO_NOTIFICATION = "REMOVE_INFO_NOTIFICATION",

  ADD_SUCCESS_NOTIFICATION = "ADD_SUCCESS_NOTIFICATION",
  REMOVE_SUCCESS_NOTIFICATION = "REMOVE_SUCCESS_NOTIFICATION",

  ADD_ERROR_NOTIFICATION = "ADD_ERROR_NOTIFICATION",
  REMOVE_ERROR_NOTIFICATION = "REMOVE_ERROR_NOTIFICATION",
}

interface IAddInfoNotification {
  type: NotificationActionTypes.ADD_INFO_NOTIFICATION;
  notification: INotification;
}

interface IRemoveInfoNotification {
  type: NotificationActionTypes.REMOVE_INFO_NOTIFICATION;
  notificationId: string;
}

interface IAddSuccessNotification {
  type: NotificationActionTypes.ADD_SUCCESS_NOTIFICATION;
  notification: INotification;
}

interface IRemoveSuccessNotification {
  type: NotificationActionTypes.REMOVE_SUCCESS_NOTIFICATION;
  notificationId: string;
}

interface IAddErrorNotification {
  type: NotificationActionTypes.ADD_ERROR_NOTIFICATION;
  notification: INotification;
}

interface IRemoveErrorNotification {
  type: NotificationActionTypes.REMOVE_ERROR_NOTIFICATION;
  notificationId: string;
}

export type NotificationAction =
  | IAddInfoNotification
  | IRemoveInfoNotification
  | IAddSuccessNotification
  | IRemoveSuccessNotification
  | IAddErrorNotification
  | IRemoveErrorNotification;
