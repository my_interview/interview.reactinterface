import { json } from "stream/consumers";
import { AppThunk } from "../..";
import IQuestion from "../../entities/IQuestion";
import snakecaseKeys from "snakecase-keys";
import { getEnv } from "../../utils/envService";

export enum QuestionActionTypes {
    FETCH_QUESTIONS_SUCCESS = "FETCH_QUESTIONS_SUCCESS",
    FETCH_QUESTIONS_FAILED = "FETCH_QUESTIONS_FAILED",

    POST_QUESTION_SUCCESS = "POST_QUESTION_SUCCESS",
    POST_QUESTION_FAILED = "POST_QUESTION_FAILED",
    CLEAR_POST_QUESTION_NOTIFICATIONS = "CLEAR_QUESTION_NOTIFICATIONS",

    GET_PAGES_COUNT_SUCCESS = "GET_PAGES_COUNT_SUCESS",
    GET_PAGES_COUNT_FAILED = "GET_PAGES_COUNT_FAILED",

    FETCH_TARGET_QUESTION_SUCCESS = "FETCH_TARGET_QUESTION_SUCCESS",
    FETCH_TARGET_QUESTION_FAILED = "FETCH_TARGET_QUESTION_FAILED",

    DELETE_QUESTION_SUCCESS = "DELETE_QUESTION_SUCCESS",
    DELETE_QUESTION_FAILED = "DELETE_QUESTION_FAILED",

    UPDATE_QUESTION_SUCCESS = "UPDATE_QUESTION_SUCCESS",
    UPDATE_QUESTION_FAILED = "UPDATE_QUESTION_FAILED",

    FETCH_QUESTIONS_BY_PATTERN_SUCCESS = "FETCH_QUESTIONS_BY_PATTERN_SUCCESS",
    FETCH_QUESTIONS_BY_PATTERN_FAILED = "FETCH_QUESTIONS_BY_PATTERN_FAILED"
}

interface IFetchQuestionsSuccessAction {
    type: QuestionActionTypes.FETCH_QUESTIONS_SUCCESS;
    questions: Array<IQuestion>;
}

interface IFetchQuestionsFailedAction {
    type: QuestionActionTypes.FETCH_QUESTIONS_FAILED;
    error: string;
}

interface IPostQuestionSuccessAction {
    type: QuestionActionTypes.POST_QUESTION_SUCCESS;
}

interface IPostQuestionFailedAction {
    type: QuestionActionTypes.POST_QUESTION_FAILED;
    error: string;
}

interface IGetPagesCountSuccessAction {
    type: QuestionActionTypes.GET_PAGES_COUNT_SUCCESS;
    pagesCount: number;
}

interface IGetPagesCountFailedAction {
    type: QuestionActionTypes.GET_PAGES_COUNT_FAILED;
    error: string;
}

interface IClearPostQuestionNotificationsAction {
    type: QuestionActionTypes.CLEAR_POST_QUESTION_NOTIFICATIONS
}

interface IFetchTargetQuestionSuccess {
    type: QuestionActionTypes.FETCH_TARGET_QUESTION_SUCCESS;
    question: IQuestion;
}

interface IFetchTargetQuestionFailed {
    type: QuestionActionTypes.FETCH_TARGET_QUESTION_FAILED;
    error: string;
}

interface IDeleteQuestionSuccess {
    type: QuestionActionTypes.DELETE_QUESTION_SUCCESS,
    questionId: string;
}

interface IDeleteQuestionFailed {
    type: QuestionActionTypes.DELETE_QUESTION_FAILED
    error: string
}

interface IUpdateQuestionSuccess {
    type: QuestionActionTypes.UPDATE_QUESTION_SUCCESS,
    question: IQuestion
}

interface IUpdateQuestionFailed {
    type: QuestionActionTypes.UPDATE_QUESTION_FAILED,
    error: string
}

interface IFetchQuestionsByPatternSuccess {
    type: QuestionActionTypes.FETCH_QUESTIONS_BY_PATTERN_SUCCESS
    questions: Array<IQuestion>
}

interface IFetchQuestionsByPatternFailed {
    type: QuestionActionTypes.FETCH_QUESTIONS_BY_PATTERN_FAILED,
    error: string
}

export type QuestionAction =
    | IFetchQuestionsSuccessAction
    | IFetchQuestionsFailedAction
    | IPostQuestionSuccessAction
    | IPostQuestionFailedAction
    | IGetPagesCountSuccessAction
    | IGetPagesCountFailedAction
    | IClearPostQuestionNotificationsAction
    | IFetchTargetQuestionSuccess
    | IFetchTargetQuestionFailed
    | IDeleteQuestionSuccess
    | IDeleteQuestionFailed
    | IUpdateQuestionSuccess
    | IUpdateQuestionFailed
    | IFetchQuestionsByPatternSuccess
    | IFetchQuestionsByPatternFailed;
