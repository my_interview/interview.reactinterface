import { AppDispatch, AppThunk } from "../..";
import ITag from "../../entities/ITag";
import { getEnv } from "../../utils/envService";

export enum TagActionTypes {
  FETCH_TAGS_SUCCESS = "FETCH_TAGS_SUCCESS",
  FETCH_TAGS_FAILED = "FETCH_TAGS_FAILED",
  POST_TAG_SUCCESS = "POST_TAG_SUCCESS",
  POST_TAG_FAILED = "POST_TAG_FAILED",
  DELETE_TAG_SUCCESS = "DELETE_TAG_SUCCESS",
  DELETE_TAG_FAILED = "DELETE_TAG_FAILED",
  CLEAR_POST_TAG_NOTIFICATIONS = "CLEAR_TAG_NOTIFICATIONS",
}

interface ITagFetchSuccessAction {
  type: TagActionTypes.FETCH_TAGS_SUCCESS;
  tags: Array<ITag>;
}

interface ITagFetchFailedAction {
  type: TagActionTypes.FETCH_TAGS_FAILED;
  error: string;
}

interface ITagPostSuccessAction {
  type: TagActionTypes.POST_TAG_SUCCESS;
  tagName: string;
}

interface ITagPostFailedAction {
  type: TagActionTypes.POST_TAG_FAILED;
  error: string;
}

interface IClearPostTagNotificationsAction {
  type: TagActionTypes.CLEAR_POST_TAG_NOTIFICATIONS;
}

interface IDeleteTagSuccessAction {
  type: TagActionTypes.DELETE_TAG_SUCCESS;
  tagId: string;
}

interface IDeleteTagFailedAction {
  type: TagActionTypes.DELETE_TAG_FAILED;
  error: string;
}

export type TagAction =
  | ITagFetchSuccessAction
  | ITagFetchFailedAction
  | ITagPostSuccessAction
  | ITagPostFailedAction
  | IClearPostTagNotificationsAction
  | IDeleteTagSuccessAction
  | IDeleteTagFailedAction;
