import { json } from "stream/consumers";
import { AppThunk } from "../..";
import snakecaseKeys from "snakecase-keys";
import { getEnv } from "../../utils/envService";

export enum ProfileActionTypes {
  CREATE_PROFILE_SUCCESS = "CREATE_PROFILE_SUCCESS",
  CREATE_PROFILE_FAILED = "CREATE_PROFILE_FAILED",

  LOGIN_PROFILE_SUCCESS = "LOGIN_PROFILE_SUCCESS",
  LOGIN_PROFILE_FAILED = "LOGIN_PROFILE_FAILED",

  LOGOUT_PROFILE = 'LOGOUT_PROFILE'
}

interface IProfileCreateSuccessAction {
  type: ProfileActionTypes.CREATE_PROFILE_SUCCESS;
  profileId: string;
}

interface IProfileCreateFailedAction {
  type: ProfileActionTypes.CREATE_PROFILE_FAILED;
  error: string;
}

interface IProfileLoginSuccessAction {
  type: ProfileActionTypes.LOGIN_PROFILE_SUCCESS;
  profileId: string;
}

interface IProfileLoginFailedAction {
  type: ProfileActionTypes.LOGIN_PROFILE_FAILED;
  error: string;
}

interface IProfileLogoutAction {
    type: ProfileActionTypes.LOGOUT_PROFILE
}

export type ProfileAction =
  | IProfileCreateSuccessAction
  | IProfileCreateFailedAction
  | IProfileLoginSuccessAction
  | IProfileLoginFailedAction
  | IProfileLogoutAction;
