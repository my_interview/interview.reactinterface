import IQuestion from "../../../entities/IQuestion";
import IQuestionAction from "./IQuestionAction";
import IQuestionState from "./IQuestionState";
import {
    QuestionAction,
    QuestionActionTypes,
} from "../../actions/questionActions";
import { act } from "react-dom/test-utils";

const initialState: IQuestionState = {
    questions: new Array<IQuestion>(),
    questionsByPattern: new Array<IQuestion>(),
    questionsRequestError: "",
    questionsRequestFailed: false,
    postQuestionSuccess: false,
    postQuestionFailed: false,
    pagesCount: 0,
    getPagesCountFailed: false,
    fetchTargetQuestionFailed: false,
    deleteQuestionFailed: false,
    updateQuestionFailed: false,
    fetchQuestionsByPatternFailed: false
};

const questionReducer = (
    state: IQuestionState = initialState,
    action: QuestionAction
): IQuestionState => {
    switch (action.type) {
        case QuestionActionTypes.FETCH_QUESTIONS_SUCCESS: {
            return {
                ...state,
                questions: action.questions ?? new Array<IQuestion>(),
                questionsRequestFailed: false,
                questionsRequestError: "",
            };
        }
        case QuestionActionTypes.FETCH_QUESTIONS_FAILED: {
            return {
                ...state,
                questions: new Array<IQuestion>(),
                questionsRequestFailed: true,
                questionsRequestError: action.error ?? "",
            };
        }
        case QuestionActionTypes.POST_QUESTION_SUCCESS: {
            return {
                ...state,
                postQuestionSuccess: true,
                postQuestionFailed: false,
                postQuestionError: undefined,
            };
        }
        case QuestionActionTypes.POST_QUESTION_FAILED: {
            return {
                ...state,
                postQuestionSuccess: false,
                postQuestionFailed: true,
                postQuestionError: action.error,
            };
        }
        case QuestionActionTypes.GET_PAGES_COUNT_SUCCESS: {
            return {
                ...state,
                pagesCount: action.pagesCount,
                getPagesCountFailed: false,
                getPagesCountError: undefined,
            };
        }
        case QuestionActionTypes.GET_PAGES_COUNT_FAILED: {
            return {
                ...state,
                pagesCount: 0,
                getPagesCountFailed: true,
                getPagesCountError: action.error,
            };
        }
        case QuestionActionTypes.CLEAR_POST_QUESTION_NOTIFICATIONS: {
            return {
                ...state,
                postQuestionSuccess: false,
                postQuestionFailed: false,
                postQuestionError: undefined
            }
        }
        case QuestionActionTypes.FETCH_TARGET_QUESTION_SUCCESS: {
            return {
                ...state,
                targetQuestion: action.question,
                fetchTargetQuestionFailed: false,
                fetchTargetQuestionError: undefined
            }
        }
        case QuestionActionTypes.FETCH_TARGET_QUESTION_FAILED: {
            return {
                ...state,
                targetQuestion: undefined,
                fetchTargetQuestionFailed: true,
                fetchTargetQuestionError: action.error
            }
        }
        case QuestionActionTypes.DELETE_QUESTION_SUCCESS: {
            return {
                ...state,
                targetQuestion: state.targetQuestion?.id === action.questionId ? undefined : state.targetQuestion,
                questions: [...state.questions.filter(x => x.id !== action.questionId)],
                deleteQuestionFailed: false,
                deleteQuestionError: undefined
            }
        }
        case QuestionActionTypes.DELETE_QUESTION_FAILED: {
            return {
                ...state,
                deleteQuestionFailed: true,
                deleteQuestionError: action.error
            }
        }
        case QuestionActionTypes.UPDATE_QUESTION_SUCCESS: {
            return {
                ...state,
                targetQuestion: action.question,
                updateQuestionFailed: false,
                updateQuestionError: undefined
            }
        }
        case QuestionActionTypes.UPDATE_QUESTION_FAILED: {
            return {
                ...state,
                updateQuestionFailed: true,
                updateQuestionError: action.error
            }
        }
        case QuestionActionTypes.FETCH_QUESTIONS_BY_PATTERN_SUCCESS: {
            return {
                ...state,
                questionsByPattern: action.questions,
                fetchQuestionsByPatternFailed: false,
                fetchTargetQuestionError: undefined
            }
        }
        case QuestionActionTypes.FETCH_QUESTIONS_BY_PATTERN_FAILED: {
            return {
                ...state,
                fetchQuestionsByPatternFailed: true,
                fetchQuestionsByPatternError: action.error
            }
        }
        default: {
            return state;
        }
    }
};

export default questionReducer;
