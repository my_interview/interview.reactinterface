import IQuestion from "../../../entities/IQuestion";

interface IQuestionState {
    questions: IQuestion[];
    questionsByPattern: IQuestion[]
    questionsRequestFailed: boolean;
    questionsRequestError: string;

    postQuestionSuccess: boolean;
    postQuestionFailed: boolean;
    postQuestionError?: string;

    pagesCount: number;
    getPagesCountFailed: boolean;
    getPagesCountError?: string;

    targetQuestion?: IQuestion;
    fetchTargetQuestionFailed: boolean,
    fetchTargetQuestionError?: string;

    deleteQuestionFailed: boolean;
    deleteQuestionError?: string;

    updateQuestionFailed: boolean;
    updateQuestionError?: string;

    fetchQuestionsByPatternFailed: boolean;
    fetchQuestionsByPatternError?: string;
}

export default IQuestionState;
