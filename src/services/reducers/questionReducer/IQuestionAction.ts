import IQuestion from "../../../entities/IQuestion";

interface IQuestionAction {
    type: string;
    questions: IQuestion[] | undefined;
    error: string | undefined;
}

export default IQuestionAction;
