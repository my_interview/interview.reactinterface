interface IProfileState {
    profileId?: string;
    createProfileError?: string;
    createProfileFailed: boolean;
}

export default IProfileState;
