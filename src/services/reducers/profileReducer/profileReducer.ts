import {
  ProfileAction,
  ProfileActionTypes,
} from "../../actions/profileActions";
import IProfileState from "./IProfileState";

const initialState: IProfileState = {
  createProfileFailed: false,
};

const profileReducer = (
  state: IProfileState = initialState,
  action: ProfileAction
): IProfileState => {
  switch (action.type) {
    case ProfileActionTypes.CREATE_PROFILE_SUCCESS: {
      return {
        ...state,
        profileId: action.profileId,
        createProfileError: undefined,
        createProfileFailed: false,
      };
    }
    case ProfileActionTypes.CREATE_PROFILE_FAILED: {
      return {
        ...state,
        profileId: undefined,
        createProfileError: action.error,
        createProfileFailed: true,
      };
    }
    case ProfileActionTypes.LOGIN_PROFILE_SUCCESS: {
      return {
        ...state,
        profileId: action.profileId,
        createProfileFailed: false,
        createProfileError: undefined,
      };
    }
    case ProfileActionTypes.LOGIN_PROFILE_FAILED: {
      return {
        ...state,
        profileId: undefined,
        createProfileFailed: true,
        createProfileError: action.error,
      };
    }
    case ProfileActionTypes.LOGOUT_PROFILE: {
      return {
        ...state,
        profileId: undefined,
        createProfileFailed: false,
        createProfileError: undefined,
      };
    }
    default: {
      return state;
    }
  }
};

export default profileReducer;
