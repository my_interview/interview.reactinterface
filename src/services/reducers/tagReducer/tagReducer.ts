import ITag from "../../../entities/ITag";
import { TagAction, TagActionTypes } from "../../actions/tagActions";
import ITagState from "./ITagState";

const initialState: ITagState = {
  tags: new Array<ITag>(),
  fetchAllTagsFailed: false,
  postTagFailed: false,
  deleteTagFailed: false,
};

const tagReducer = (
  state: ITagState = initialState,
  action: TagAction
): ITagState => {
  switch (action.type) {
    case TagActionTypes.FETCH_TAGS_SUCCESS: {
      return {
        ...state,
        tags: action.tags,
        fetchAllTagsFailed: false,
        fetchAllTagsError: undefined,
      };
    }
    case TagActionTypes.FETCH_TAGS_FAILED: {
      return {
        ...state,
        tags: new Array<ITag>(),
        fetchAllTagsFailed: true,
        fetchAllTagsError: action.error,
      };
    }
    case TagActionTypes.POST_TAG_SUCCESS: {
      return {
        ...state,
        lastSuccessPostedTag: action.tagName,
        postTagFailed: false,
        postTagError: undefined,
      };
    }
    case TagActionTypes.POST_TAG_FAILED: {
      return {
        ...state,
        postTagFailed: true,
        postTagError: action.error,
        lastSuccessPostedTag: undefined,
      };
    }
    case TagActionTypes.CLEAR_POST_TAG_NOTIFICATIONS: {
      return {
        ...state,
        postTagFailed: false,
        postTagError: undefined,
        lastSuccessPostedTag: undefined,
      };
    }
    case TagActionTypes.DELETE_TAG_FAILED: {
      return {
        ...state,
        deleteTagFailed: true,
        deleteTagError: action.error
      };
    }
    case TagActionTypes.DELETE_TAG_SUCCESS: {
      return {
        ...state,
        tags: state.tags.filter(x => x.id !== action.tagId),
        deleteTagFailed: false,
        deleteTagError: undefined
      };
    }
    default:
      return state;
  }
};

export default tagReducer;
