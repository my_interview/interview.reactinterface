import ITag from "../../../entities/ITag";

interface ITagState {
    tags: Array<ITag>,
    lastSuccessPostedTag?: string,
    fetchAllTagsFailed: boolean,
    fetchAllTagsError?: string,
    postTagFailed: boolean,
    postTagError?: string,
    deleteTagFailed: boolean,
    deleteTagError?: string
}

export default ITagState;
