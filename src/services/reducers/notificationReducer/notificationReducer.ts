import INotification from "../../../models/INotification";
import { NotificationAction, NotificationActionTypes } from "../../actions/notificationActions";
import INotificationState from "./INotificationState";

const initialState: INotificationState = {
    infoNotifications: new Array<INotification>(),
    successNotifications: new Array<INotification>(),
    errorNotifications: new Array<INotification>()
}

const notificationReducer = (state: INotificationState = initialState, action: NotificationAction): INotificationState => {
    switch(action.type) {
        case NotificationActionTypes.ADD_INFO_NOTIFICATION: {
            return {
                ...state,
                infoNotifications: [...state.infoNotifications, action.notification]
            }
        }
        case NotificationActionTypes.REMOVE_INFO_NOTIFICATION: {
            return {
                ...state,
                infoNotifications: [...state.infoNotifications.filter(x => x.id !== action.notificationId)]
            }
        }
        case NotificationActionTypes.ADD_SUCCESS_NOTIFICATION: {
            return {
                ...state,
                successNotifications: [...state.successNotifications, action.notification]
            }
        }
        case NotificationActionTypes.REMOVE_SUCCESS_NOTIFICATION: {
            return {
                ...state,
                successNotifications: [...state.infoNotifications.filter(x => x.id !== action.notificationId)]
            }
        }
        case NotificationActionTypes.ADD_ERROR_NOTIFICATION: {
            return {
                ...state,
                errorNotifications: [...state.errorNotifications, action.notification]
            }
        }
        case NotificationActionTypes.REMOVE_ERROR_NOTIFICATION: {
            return {
                ...state,
                errorNotifications: [...state.errorNotifications.filter(x => x.id !== action.notificationId)]
            }
        }
        default: {
            return state;
        }
    }
}

export default notificationReducer;