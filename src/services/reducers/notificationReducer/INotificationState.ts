import INotification from "../../../models/INotification";

interface INotificationState {
    infoNotifications: Array<INotification>,
    successNotifications: Array<INotification>,
    errorNotifications: Array<INotification>
};

export default INotificationState;