import { combineReducers } from "redux";
import notificationReducer from "./notificationReducer/notificationReducer";
import profileReducer from "./profileReducer/profileReducer";
import questionReducer from "./questionReducer/questionReducer";
import tagReducer from "./tagReducer/tagReducer";

export const rootReducer = combineReducers({
    questionReducer: questionReducer,
    profileReducer: profileReducer,
    tagReducer: tagReducer,
    notificationReducer: notificationReducer
});

export type RootState = ReturnType<typeof rootReducer>;
