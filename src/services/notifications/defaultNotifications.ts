import { iNotification, Store } from "react-notifications-component";
import IDefaultNotification from "../../models/IDefaultNotification";

export const sendSuccessNotification = ({ title, message }: IDefaultNotification) => {
  Store.addNotification({
    type: "success",
    title: title,
    message: message,
    ...getDefaultSettings()
  });
};

export const sendErrorNotification = ({ title, message }: IDefaultNotification) => {
  Store.addNotification({
    type: "danger",
    title: title,
    message: message,
    ...getDefaultSettings()
  });
};

export const sendInfoNotification = ({ title, message }: IDefaultNotification) => {
  Store.addNotification({
    type: "info",
    title: title,
    message: message,
    ...getDefaultSettings()
  });
};

const getDefaultSettings = (): iNotification => {
    return {
        container: "bottom-right",
        animationIn: ["animate__animated", "animate__fadeIn"],
        animationOut: ["animate__animated", "animate__fadeOut"],
        dismiss: {
          duration: 5000,
        }
    }
};
