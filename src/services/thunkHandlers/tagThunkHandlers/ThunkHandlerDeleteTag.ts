import axios from "axios";
import snakecaseKeys from "snakecase-keys";
import { NotificationAction } from "../../actions/notificationActions";
import { TagAction, TagActionTypes } from "../../actions/tagActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerDeleteTag extends ThunkHandlerBase<TagAction, void> {
  protected successNotificationMessage: string;
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;
  protected tagId: string;

  constructor(
    dispatch: (action: TagAction | NotificationAction) => void,
    tagId: string
  ) {
    super(dispatch);

    this.successNotificationMessage = "Тэг удалён";
    this.isNotifyOnFailed = true;
    this.isNotifyOnSuccess = true;
    this.tagId = tagId;
  }

  protected HandleRequest(): Promise<void> {
    return axios.delete(`${this.baseUrl}/api/v1/tags/${this.tagId}`);
  }

  protected DispatchOnSuccess(response: void): void {
    this.dispatch({
      type: TagActionTypes.DELETE_TAG_SUCCESS,
      tagId: this.tagId,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({
        type: TagActionTypes.DELETE_TAG_FAILED,
        error: error
    });
  }
}

export default ThunkHandlerDeleteTag;
