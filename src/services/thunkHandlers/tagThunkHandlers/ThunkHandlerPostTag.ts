import axios from "axios";
import { NotificationAction } from "../../actions/notificationActions";
import { TagAction, TagActionTypes } from "../../actions/tagActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerPostTag extends ThunkHandlerBase<TagAction, void> {
  protected successNotificationMessage: string;
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;

  private readonly tagName: string;

  constructor(
    dispatch: (action: TagAction | NotificationAction) => void,
    tagName: string
  ) {
    super(dispatch);

    this.tagName = tagName;
    this.successNotificationMessage = "Тэг добавлен";
    this.isNotifyOnFailed = true;
    this.isNotifyOnSuccess = true;
  }

  protected async HandleRequest(): Promise<void> {
    const body = {
      name: this.tagName,
    };

    await axios.post(
      `${this.baseUrl}/api/v1/tags`,
      { ...body },
      { headers: { "Content-Type": "application/json" } }
    );
  }

  protected DispatchOnSuccess(response: void): void {
    this.dispatch({
      type: TagActionTypes.POST_TAG_SUCCESS,
      tagName: this.tagName,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({ type: TagActionTypes.POST_TAG_FAILED, error: error });
  }
}

export default ThunkHandlerPostTag;
