import axios from "axios";
import ITag from "../../../entities/ITag";
import { NotificationAction } from "../../actions/notificationActions";
import { TagAction, TagActionTypes } from "../../actions/tagActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerFetchAllTags extends ThunkHandlerBase<
  TagAction,
  Array<ITag>
> {
  protected successNotificationMessage: string;
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;

  constructor(dispatch: (action: TagAction | NotificationAction) => void) {
    super(dispatch);

    this.successNotificationMessage = "";
    this.isNotifyOnFailed = false;
    this.isNotifyOnSuccess = false;
  }

  protected async HandleRequest(): Promise<ITag[]> {
    const result = await axios.get<Array<ITag>>(`${this.baseUrl}/api/v1/tags`);

    return result.data;
  }

  protected DispatchOnSuccess(tags: ITag[]): void {
    this.dispatch({
      type: TagActionTypes.FETCH_TAGS_SUCCESS,
      tags: tags,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({ type: TagActionTypes.FETCH_TAGS_FAILED, error: error });
  }
}

export default ThunkHandlerFetchAllTags;
