import axios from "axios";
import snakecaseKeys from "snakecase-keys";
import { AppDispatch } from "../../..";
import { getEnv } from "../../../utils/envService";
import { NotificationAction } from "../../actions/notificationActions";
import {
  QuestionAction,
  QuestionActionTypes,
} from "../../actions/questionActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerDeleteQuestion extends ThunkHandlerBase<
  QuestionAction,
  unknown
> {
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;
  protected successNotificationMessage: string;

  private readonly questionId: string;
  private readonly profileId: string;

  constructor(
    dispatch: (action: QuestionAction | NotificationAction) => void,
    questionId: string,
    profileId: string
  ) {
    super(dispatch);

    this.successNotificationMessage = "Вопрос удалён";
    this.isNotifyOnFailed = true;
    this.isNotifyOnSuccess = true;
    this.questionId = questionId;
    this.profileId = profileId;
  }

  protected async HandleRequest(): Promise<unknown> {
    return axios.delete(`${this.baseUrl}/api/v1/questions`, {
      headers: { "Content-Type": "application/json" },
      data: snakecaseKeys({ id: this.questionId, profileId: this.profileId }),
    });
  }

  protected DispatchOnSuccess(response: unknown): void {
    console.log(this.questionId);
    this.dispatch({
      type: QuestionActionTypes.DELETE_QUESTION_SUCCESS,
      questionId: this.questionId,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({
      type: QuestionActionTypes.DELETE_QUESTION_FAILED,
      error: error,
    });
  }
}

export default ThunkHandlerDeleteQuestion;
