import axios, { AxiosResponse } from "axios";
import camelcaseKeys from "camelcase-keys";
import IQuestion from "../../../entities/IQuestion";
import { NotificationAction } from "../../actions/notificationActions";
import { QuestionAction, QuestionActionTypes } from "../../actions/questionActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerFetchTargetQuestion extends ThunkHandlerBase<
  QuestionAction,
  IQuestion
> {
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;
  protected successNotificationMessage: string;

  private readonly questionId: string;
  private readonly profileId?: string;

  constructor(
    dispatch: (action: QuestionAction | NotificationAction) => void,
    questionId: string,
    profileId?: string
  ) {
    super(dispatch);

    this.questionId = questionId;
    this.profileId = profileId;
    this.successNotificationMessage = "";
    this.isNotifyOnFailed = false;
    this.isNotifyOnSuccess = false;
  }

  protected async HandleRequest(): Promise<IQuestion> {
    const result = await axios.get<IQuestion>(
      `${this.baseUrl}/api/v1/questions/${this.questionId}/${this.profileId ?? ""}`
    );

    return result.data;
  }

  protected DispatchOnSuccess(question: IQuestion): void {
    this.dispatch({
      type: QuestionActionTypes.FETCH_TARGET_QUESTION_SUCCESS,
      question: question,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({
      type: QuestionActionTypes.FETCH_TARGET_QUESTION_FAILED,
      error: error,
    });
  }
}

export default ThunkHandlerFetchTargetQuestion;