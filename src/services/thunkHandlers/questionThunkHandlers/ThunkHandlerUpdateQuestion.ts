import axios from "axios";
import snakecaseKeys from "snakecase-keys";
import IQuestion from "../../../entities/IQuestion";
import { NotificationAction } from "../../actions/notificationActions";
import { QuestionAction, QuestionActionTypes } from "../../actions/questionActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerUpdateQuestion extends ThunkHandlerBase<QuestionAction, void> {
    protected successNotificationMessage: string;
    protected isNotifyOnSuccess: boolean;
    protected isNotifyOnFailed: boolean;

    private question: IQuestion;

    constructor(dispatch: (action: QuestionAction | NotificationAction) => void, question: IQuestion) {
        super(dispatch);
        
        this.question = question;
        this.isNotifyOnSuccess = true;
        this.isNotifyOnFailed = true;
        this.successNotificationMessage = "Вопрос обновлен";
    }

    protected HandleRequest(): Promise<void> {
        const headers = { 
            'Content-Type': 'application/json',
        };
        console.log(this.question);
        return axios.put(`${this.baseUrl}/api/v1/questions`, snakecaseKeys({...this.question, tags: this.question.tags.map(x => x.id)}), { headers });
    }

    protected DispatchOnSuccess(response: void): void {
        this.dispatch({ type: QuestionActionTypes.UPDATE_QUESTION_SUCCESS, question: this.question });
    }

    protected DispatchOnFailed(error: string): void {
        this.dispatch({ type: QuestionActionTypes.UPDATE_QUESTION_FAILED, error: error });
    }

}

export default ThunkHandlerUpdateQuestion;