import axios from "axios";
import IQuestion from "../../../entities/IQuestion";
import { NotificationAction } from "../../actions/notificationActions";
import { QuestionAction, QuestionActionTypes } from "../../actions/questionActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerFetchQuestionsByPattern extends ThunkHandlerBase<QuestionAction, Array<IQuestion>> {
    protected successNotificationMessage: string;
    protected isNotifyOnSuccess: boolean;
    protected isNotifyOnFailed: boolean;
    protected pattern: string;

    constructor(dispatch: (action: QuestionAction | NotificationAction) => void, pattern: string) {
        super(dispatch);
        
        this.pattern = pattern;
        this.successNotificationMessage = "";
        this.isNotifyOnFailed = false;
        this.isNotifyOnSuccess = false;
    }

    protected async HandleRequest(): Promise<IQuestion[]> {
        const result = await axios.get<Array<IQuestion>>(
            `${this.baseUrl}/api/v1/questions/all/${this.pattern}`
        );
    
        return result.data;
    }

    protected DispatchOnSuccess(questions: IQuestion[]): void {
        this.dispatch({ type: QuestionActionTypes.FETCH_QUESTIONS_BY_PATTERN_SUCCESS, questions: questions });
    }

    protected DispatchOnFailed(error: string): void {
        this.dispatch({ type: QuestionActionTypes.FETCH_QUESTIONS_BY_PATTERN_FAILED, error: error });
    }
}

export default ThunkHandlerFetchQuestionsByPattern;