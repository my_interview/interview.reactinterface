import axios from "axios";
import { NotificationAction } from "../../actions/notificationActions";
import {
  QuestionAction,
  QuestionActionTypes,
} from "../../actions/questionActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerGetPageCount extends ThunkHandlerBase<
  QuestionAction,
  number
> {
  protected successNotificationMessage: string;
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;

  constructor(dispatch: (action: QuestionAction | NotificationAction) => void) {
    super(dispatch);

    this.successNotificationMessage = "";
    this.isNotifyOnSuccess = false;
    this.isNotifyOnFailed = false;
  }

  protected async HandleRequest(): Promise<number> {
    const result = await axios.get<number>(`${this.baseUrl}/api/v1/questions/pages`);

    return result.data;
  }

  protected DispatchOnSuccess(pagesCount: number): void {
    this.dispatch({
      type: QuestionActionTypes.GET_PAGES_COUNT_SUCCESS,
      pagesCount: pagesCount,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({
      type: QuestionActionTypes.GET_PAGES_COUNT_FAILED,
      error: error,
    })
  }
}

export default ThunkHandlerGetPageCount;