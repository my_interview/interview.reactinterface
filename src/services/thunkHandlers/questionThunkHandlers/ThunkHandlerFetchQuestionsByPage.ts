import axios from "axios";
import IQuestion from "../../../entities/IQuestion";
import { NotificationAction } from "../../actions/notificationActions";
import {
  QuestionAction,
  QuestionActionTypes,
} from "../../actions/questionActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerFetchQuestionsByPage extends ThunkHandlerBase<
  QuestionAction,
  Array<IQuestion>
> {
  protected successNotificationMessage: string;
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;

  private readonly pageNumber: number;

  constructor(
    dispatch: (action: QuestionAction | NotificationAction) => void,
    pageNumber: number
  ) {
    super(dispatch);

    this.pageNumber = pageNumber;
    this.successNotificationMessage = "";
    this.isNotifyOnFailed = false;
    this.isNotifyOnSuccess = false;
  }

  protected async HandleRequest(): Promise<IQuestion[]> {
    const result = await axios.get<Array<IQuestion>>(
      `${this.baseUrl}/api/v1/questions?page=${this.pageNumber}`
    );

    return result.data;
  }

  protected DispatchOnSuccess(questions: IQuestion[]): void {
    this.dispatch({
      type: QuestionActionTypes.FETCH_QUESTIONS_SUCCESS,
      questions: questions,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({
      type: QuestionActionTypes.FETCH_QUESTIONS_FAILED,
      error: error,
    });
  }
}

export default ThunkHandlerFetchQuestionsByPage;
