import axios from "axios";
import snakecaseKeys from "snakecase-keys";
import IQuestion from "../../../entities/IQuestion";
import { NotificationAction } from "../../actions/notificationActions";
import {
  QuestionAction,
  QuestionActionTypes,
} from "../../actions/questionActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerPostQuestion extends ThunkHandlerBase<QuestionAction, void> {
  protected successNotificationMessage: string;
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;

  private readonly question: IQuestion;

  constructor(
    dispatch: (action: QuestionAction | NotificationAction) => void,
    question: IQuestion
  ) {
    super(dispatch);

    this.question = question;
    this.successNotificationMessage = "Вопрос добавлен";
    this.isNotifyOnFailed = true;
    this.isNotifyOnSuccess = true;
  }

  protected async HandleRequest(): Promise<void> {
    console.log(this.question);
    await axios.post(
      `${this.baseUrl}/api/v1/questions`,
      {
        ...snakecaseKeys({
          ...this.question,
          tags: this.question.tags.map((t) => t.id),
        }),
      },
      {
        headers: { "Content-Type": "application/json" },
      }
    );
  }

  protected DispatchOnSuccess(response: void): void {
    this.dispatch({ type: QuestionActionTypes.POST_QUESTION_SUCCESS });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({
      type: QuestionActionTypes.POST_QUESTION_FAILED,
      error: error,
    });
  }
}

export default ThunkHandlerPostQuestion;
