import axios from "axios";
import { NotificationAction } from "../../actions/notificationActions";
import {
  ProfileAction,
  ProfileActionTypes,
} from "../../actions/profileActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerCreateProfile extends ThunkHandlerBase<
  ProfileAction,
  string
> {
  protected successNotificationMessage: string;
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;

  constructor(dispatch: (action: ProfileAction | NotificationAction) => void) {
    super(dispatch);

    this.successNotificationMessage = "Профиль создан";
    this.isNotifyOnFailed = true;
    this.isNotifyOnSuccess = true;
  }

  protected async HandleRequest(): Promise<string> {
    const result = await axios.post(
      `${this.baseUrl}/api/v1/profiles`,
      {},
      { headers: { ContentType: "application/json" } }
    );

    return result.data;
  }

  protected DispatchOnSuccess(profileId: string): void {
    this.dispatch({
      type: ProfileActionTypes.LOGIN_PROFILE_SUCCESS,
      profileId: profileId,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({
      type: ProfileActionTypes.LOGIN_PROFILE_FAILED,
      error: error,
    });
  }
}

export default ThunkHandlerCreateProfile;
