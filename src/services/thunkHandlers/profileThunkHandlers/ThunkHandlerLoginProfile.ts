import axios from "axios";
import { NotificationAction } from "../../actions/notificationActions";
import {
  ProfileAction,
  ProfileActionTypes,
} from "../../actions/profileActions";
import ThunkHandlerBase from "../ThunkHandlerBase";

class ThunkHandlerLoginProfile extends ThunkHandlerBase<ProfileAction, void> {
  protected successNotificationMessage: string;
  protected isNotifyOnSuccess: boolean;
  protected isNotifyOnFailed: boolean;

  private readonly profileId: string;

  constructor(
    dispatch: (action: ProfileAction | NotificationAction) => void,
    profileId: string
  ) {
    super(dispatch);

    this.profileId = profileId;
    this.successNotificationMessage = "Вы успешно вошли";
    this.isNotifyOnFailed = true;
    this.isNotifyOnSuccess = true;
  }

  protected async HandleRequest(): Promise<void> {
    await axios.head(`${this.baseUrl}/api/v1/profiles/${this.profileId}`);
  }

  protected DispatchOnSuccess(response: void): void {
    this.dispatch({
      type: ProfileActionTypes.LOGIN_PROFILE_SUCCESS,
      profileId: this.profileId,
    });
  }

  protected DispatchOnFailed(error: string): void {
    this.dispatch({
      type: ProfileActionTypes.LOGIN_PROFILE_FAILED,
      error: error,
    });
  }
}

export default ThunkHandlerLoginProfile;
