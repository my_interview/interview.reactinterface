import axios from "axios";
import { AppDispatch } from "../..";
import {
  NotificationActionTypes,
  NotificationAction,
} from "../actions/notificationActions";
import { v4 as uuidv4 } from "uuid";
import { getEnv } from "../../utils/envService";

abstract class ThunkHandlerBase<TAction, TRequestResponse> {
  protected readonly dispatch: (action: TAction | NotificationAction) => void;
  protected readonly baseUrl: string;
  protected readonly abstract successNotificationMessage: string;
  protected readonly abstract isNotifyOnSuccess: boolean;
  protected readonly abstract isNotifyOnFailed: boolean;

  constructor(dispatch: (action: TAction | NotificationAction) => void) {
    this.dispatch = dispatch;
    this.baseUrl = getEnv("REACT_APP_API_URL");
  }

  protected abstract HandleRequest(): Promise<TRequestResponse>;
  protected abstract DispatchOnSuccess(response: TRequestResponse): void;
  protected abstract DispatchOnFailed(error: string): void;

  public async Handle(): Promise<void> {
    try {
      const response = await this.HandleRequest();

      this.DispatchOnSuccess(response);

      if (this.isNotifyOnSuccess) {
        this.dispatch({
          type: NotificationActionTypes.ADD_SUCCESS_NOTIFICATION,
          notification: { id: uuidv4(), message: this.successNotificationMessage },
        });
      }
    } catch (error: unknown) {
      let errorMessage = "Unknown error";

      if (axios.isAxiosError(error)) {
        errorMessage = error.message;
      }

      if (typeof error === "string") {
        errorMessage = error;
      }

      this.DispatchOnFailed(errorMessage);

      if (this.isNotifyOnFailed) {
        this.dispatch({
          type: NotificationActionTypes.ADD_ERROR_NOTIFICATION,
          notification: { id: uuidv4(), message: errorMessage },
        });
      }
    }
  }
}

export default ThunkHandlerBase;
